class SampleClass():
	def __init__(self, year):
		self.year = year
	def show_year(self):
		print(f'The year is: {self.year}')

myObj = SampleClass(2020) 
print(myObj.year)
myObj.show_year()


# [Section] Encapsulation
class Person():
    def __init__(self):
        self._name = "John Doe"
        self._age = 0
    
    def set_name(self, name):
        self._name = name

    def get_name(self):
        print(f'Name of Person: {self._name}')

    def set_age(self, age):
        self._age = age

    def get_age(self):
        print(f'Age of Person: {self._age}')

p1 = Person()
# print(p1.name) 
p1.get_name()
p1.set_name("Bob Doe")
p1.get_name()


# Test Cases:
p1.set_age(38)
p1.get_age()

# [Section] Inheritance
class Employee(Person):
    def __init__(self, employeeId):
        super().__init__()
        self._employeeId = employeeId
    
    def get_employeeId(self):
        print(f"The Employee ID is {self._employeeId}")

    def set_employeeId(self, employeeId):
        self._employeeId = employeeId

    def get_details(self):
        print(f"{self._employeeId} belongs to {self._name}")

emp1 = Employee("Emp-001")
emp1.get_details()
emp1.set_name("Bob Doe")
emp1.set_age(38)
emp1.get_details()


class Student(Person):
    def __init__(self, studentNo, course, year_level):
        super().__init__()
        self._studentNo = studentNo
        self._course = course
        self._year_level = year_level
    
    def get_studentNo(self):
        print(f"Student number of Student is {self._studentNo}")

    def get_course(self):
        print(f"Course of Student is {self._course}")

    def get_year_level(self):
        print(f"The Year Level of Student is {self._year_level}")

    def set_studentNo(self, studentNo):
        self._studentNo = studentNo
    
    def set_course(self, course):
        self._course = course
    
    def set_year_level(self, year_level):
        self._year_level = year_level

    def get_details(self):
        print(f"{self._name} is currently in year {self._year_level} taking up {self._course}.")

student1 = Student("stdt-001", "Computer Science", 1)
student1.set_name("Brandon Smith")
student1.set_age(18)
student1.get_details()

# [Section] Polymorphism
class Admin():
	def is_admin(self):
		print(True)

	def user_type(self):
		print('Admin User')

class Customer():
	def is_admin(self):
		print(False)

	def user_type(self):
		print('Regular User')

def test_function(obj):
	obj.is_admin()
	obj.user_type()

# Create object instances for Admin and Customer
user_admin = Admin()
user_customer = Customer()

test_function(user_admin)
test_function(user_customer)

# Polymorphism with Class Methods
class TeamLead():
	def occupation(self):
		print('Team Lead')

	def hasAuth(self):
		print(True)


class TeamMember():
	def occupation(self):
		print('Team Member')

	def hasAuth(self):
		print(False)

tl1 = TeamLead()
tm1 = TeamMember()

for person in (tl1, tm1):
    person.occupation()

# Polymorphism with Inheritance
class Zuitt():
	def tracks(self):
		print('We are currently offering 3 tracks(developer career, pi-shape career, and short courses)')

	def num_of_hours(self):
		print('Learn web development in 360 hours!')


class DeveloperCareer(Zuitt):
	def num_of_hours(self):
		print('Learn the basics of web development in 240 hours!')


class PiShapedCareer(Zuitt):
	def num_of_hours(self):
		print('Learn skills for no-code app development in 140 hours!')


class ShortCourses(Zuitt):
	def num_of_hours(self):
		print('Learn advanced topics in web development in 20 hours!')

course1 = DeveloperCareer()
course2 = PiShapedCareer()
course3 = ShortCourses()

course1.num_of_hours()
course2.num_of_hours()
course3.num_of_hours()

# [Section] Abstraction
from abc import ABC, abstractclassmethod 

# inherits the abstract class 
class Polygon(ABC):

    @abstractclassmethod
    def printNumberOfSides(self):
        pass

class Triangle(Polygon):
    def __init__(self):
        super().__init__()
    def printNumberOfSides(self):
        print(f"This polygon has 3 sides.")


class Pentagon(Polygon):
    def __init__(self):
        super().__init__()

    def printNumberOfSides(self):
        print(f"This polygon has 5 sides.")

shape1 = Triangle()
shape2 = Pentagon()
shape1.printNumberOfSides()
shape2.printNumberOfSides()